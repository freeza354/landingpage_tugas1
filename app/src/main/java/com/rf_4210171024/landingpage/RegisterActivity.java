package com.rf_4210171024.landingpage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    Button backBtn;
    Button registerBtn;

    EditText emailText;

    public static String EMAIL_TEXT = "default";
    public static int RESULT_CODE = 110;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        backBtn = findViewById(R.id.backBtn);
        backBtn.setOnClickListener(this);

        registerBtn = findViewById(R.id.create_accountBtn);
        registerBtn.setOnClickListener(this);

        emailText = findViewById(R.id.editEmail);
    }

    @Override
    public void onClick(View v){
        switch (v.getId()){
            case R.id.backBtn:
                Intent moveIntent = new Intent(RegisterActivity.this, MainActivity.class);
                startActivity(moveIntent);
                break;
            case R.id.create_accountBtn:
                String textVal = emailText.getText().toString();
                if (textVal != null){
                    Intent resultIntent = new Intent();
                    resultIntent.putExtra(EMAIL_TEXT, textVal);
                    setResult(RESULT_CODE, resultIntent);
                    finish();
                }
                break;
        }
    }

}
