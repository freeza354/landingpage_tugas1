package com.rf_4210171024.landingpage;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class GraphicsActivity extends AppCompatActivity {

    ImageView ourView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        draw();

        setContentView(ourView);
    }

    public void draw(){
        Bitmap blankBitmap;
        blankBitmap = Bitmap.createBitmap(600, 600, Bitmap.Config.ARGB_8888);

        Canvas canvas;
        canvas = new Canvas(blankBitmap);

        ourView = new ImageView(this);
        ourView.setImageBitmap(blankBitmap);

        Paint paint;
        paint = new Paint();

        canvas.drawColor(Color.argb(255,255,255,255));
        paint.setColor(Color.argb(255,26,128,182));

        Bitmap bitmapImage;
        bitmapImage = BitmapFactory.decodeResource(this.getResources(), R.drawable.no_data_recv);
        canvas.drawBitmap(bitmapImage, 150, 200, paint);

        canvas.drawLine(50,50,250,250, paint);
        canvas.drawText("Logged in",50,50, paint);
        canvas.drawPoint(40,50,paint);

        paint.setColor(Color.argb(255,249,129,0));
        canvas.drawRect(50, 450,500,550,paint);
        canvas.drawCircle(40,250,50, paint);
        canvas.drawCircle(500,250,50, paint);
    }


}
